package com.example.social.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.social.R;
import com.example.social.model.Post;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import static com.example.social.networking.NetworkingHelper.POST_URL;

public class HomeActivity extends AppCompatActivity {

    private LinearLayout content;
    private Button createPost, refreshPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        content = (LinearLayout) findViewById(R.id.content);
        createPost = (Button) findViewById(R.id.create_post);
        refreshPosts = (Button) findViewById(R.id.refresh_posts);
        getPosts();

        refreshPosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                content.removeAllViews();
                getPosts();
            }
        });

        createPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreatePostActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getPosts() {
        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.get(POST_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                Post post = getPost(response.getJSONObject(i));
                                content.addView(getPostView(post));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_msg), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private Post getPost(JSONObject object) throws JSONException {
        String id = object.getString("id");
        String title = object.getString("title");
        String message = object.getString("message");
        String imageUrl = object.getString("image_url");
        return new Post(id, title, message, imageUrl);
    }

    private View getPostView(final Post post) {
        View child = getLayoutInflater().inflate(R.layout.item_social, null);

        final RelativeLayout root = child.findViewById(R.id.root);
        TextView title = child.findViewById(R.id.title);
        ImageView image = child.findViewById(R.id.image);

        title.setText(post.getTitle());
        Picasso.get()
                .load(post.getImageUrl())
                .into(image);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditPostActivity.class);
                intent.putExtra("post", post);
                startActivity(intent);
            }
        });
        return child;
    }

}
