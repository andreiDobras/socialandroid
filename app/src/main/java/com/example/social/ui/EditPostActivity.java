package com.example.social.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.social.R;
import com.example.social.model.Post;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import static com.example.social.networking.NetworkingHelper.POST_URL;

public class EditPostActivity extends AppCompatActivity {

    private TextView titleTxt, messageTxt;
    private ImageView image;
    private Button delete, update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        titleTxt = (TextView) findViewById(R.id.title_text);
        messageTxt = (TextView) findViewById(R.id.message_text);
        image = (ImageView) findViewById(R.id.image);

        delete = (Button) findViewById(R.id.delete);
        update = (Button) findViewById(R.id.update_post);

        final Post post = getIntent().getParcelableExtra("post");

        titleTxt.setText(post.getTitle());
        messageTxt.setText(post.getMessage());
        Picasso.get()
                .load(post.getImageUrl())
                .into(image);


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePost(post);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePost(post);
            }
        });
    }

    private void deletePost(Post post) {
        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.delete(POST_URL + post.getId())
                .setPriority(Priority.HIGH)
                .addBodyParameter("title", titleTxt.getText().toString())
                .addBodyParameter("message", messageTxt.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), getString(R.string.delete_msg), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    private void updatePost(Post post) {
        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.patch(POST_URL + post.getId())
                .setPriority(Priority.HIGH)
                .addBodyParameter("title", titleTxt.getText().toString())
                .addBodyParameter("message", messageTxt.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), getString(R.string.update_msg), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
