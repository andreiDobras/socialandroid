package com.example.social.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.social.R;
import com.example.social.model.Post;

import org.json.JSONObject;

import static com.example.social.networking.NetworkingHelper.POST_URL;

public class CreatePostActivity extends AppCompatActivity {

    private TextView titleTxt, messageTxt;
    private Button cancel, create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);

        titleTxt = (TextView) findViewById(R.id.title_text);
        messageTxt = (TextView) findViewById(R.id.message_text);

        cancel = (Button) findViewById(R.id.cancel);
        create = (Button) findViewById(R.id.create_post);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!titleTxt.equals("") && !messageTxt.equals("")) {
                    createPost();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.all_fields_msg), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void createPost() {
        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.post(POST_URL)
                .setPriority(Priority.HIGH)
                .addBodyParameter("title", titleTxt.getText().toString())
                .addBodyParameter("message", messageTxt.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), getString(R.string.create_msg), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
